// Search modal
const searchBtn = document.querySelector(".search-btn");
const searchModal = document.getElementById("search-modal");
const searchCloseBtn = document.getElementById("search-close-btn");

searchBtn.addEventListener("click", () => {
    searchModal.classList.add("active");
    const searchInput= document.getElementById("search-input");
    searchInput.focus();
});

searchCloseBtn.addEventListener("click", () => {
  searchModal.classList.remove("active");
});
document.addEventListener("keydown", (event) => {
  if (event.key === "Escape") {
    // Esc?
    searchModal.classList.remove("active"); // close form
  }
});

// Mobile menu
const barBtn = document.querySelector(".navbar-toggler");
const mobileMenu = document.getElementById("navcol-3");
const menuOverlay = document.getElementById("menu-overlay");
const menuCloseBtn = document.getElementById("menu-close-btn");

barBtn.addEventListener("click", () => {
  mobileMenu.classList.add("active");
  menuOverlay.classList.add("active");
});

menuCloseBtn.addEventListener("click", () => {
  mobileMenu.classList.remove("active");
  menuOverlay.classList.remove("active");
});

menuOverlay.addEventListener("click", () => {
  mobileMenu.classList.remove("active");
  menuOverlay.classList.remove("active");
});

const header = document.getElementById("header");

const headerOffset = header.offsetTop;

window.addEventListener("scroll", () => {
  if (window.pageYOffset > headerOffset) {
    header.classList.add("sticky"); // add class sticky
  } else {
    header.classList.remove("sticky"); // remove  class sticky
  }
});


